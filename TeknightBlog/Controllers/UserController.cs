using System.Web.Mvc;
using TeknightBlog.Models;

namespace TeknightBlog.Controllers
{
    public class UserController : Controller
    {
        // Used to store which page the user should be redirected to after loggin in or out
        public void SetRedirectPage()
        {
            if (!string.IsNullOrEmpty(Request?.UrlReferrer?.ToString()))
            {
                Session["Redirect"] = Request.UrlReferrer.ToString();
            }
            else
            {
                Session["Redirect"] = "Blog";
            }
        }

        public ActionResult LogIn()
        {
            SetRedirectPage();
            return View();
        }

        public ActionResult LogOut()
        {
            Session["User"] = null;
            SetRedirectPage();
            return Redirect((string)Session["Redirect"]);
        }

        [HttpPost]
        public ActionResult LogIn(string username, string password)
        {
            User user = Global.DbHandler.GetFirst<User>(x => x.Username == username);
            if (user == null || !user.CheckPassword(password))
            {
                ViewBag.ErrorMsg = "Invalid login data";
                return View();
            }
            Session["User"] = user;
            return Redirect((string)Session["Redirect"]);
        }

        public ActionResult New()
        {
            return View();
        }

        [HttpPost]
        public ActionResult New(User user)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ErrorMsg = "Invalid data";
                return View();
            }

            Global.DbHandler.Add(user);

            Session["User"] = user;
            return RedirectToAction("Front", "Blog");
        }
    }
}