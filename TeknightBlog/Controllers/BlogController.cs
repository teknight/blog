using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TeknightBlog.Models;

namespace TeknightBlog.Controllers
{
    // Class must extend Controller
    public class BlogController : Controller
    {
        // Show permissions denied, if user is not admin
        public ActionResult BadPermission() => NotAdmin() ? View("PermissionDenied") : null;

        // Return wheter the current user is admin
        public bool NotAdmin()
        {
            User user;
            return NotAdmin(out user);
        }

        // Return wheter the current user is admin. Out's the current user for further processing
        public bool NotAdmin(out User user)
        {
            user = Session["User"] as User;
            return (user == null || user.Admin == false);
        }

        // Return wheter post exist. Out post for further processing if exist
        public bool NotPost(string urlTitle, out Post post)
        {
            post = Global.DbHandler.GetFirst<Post>(x => x.UrlTitle == urlTitle);
            return (post == null);
        }

        public ActionResult Front()
        {
            // Pass all posts to the front page view
            return View(Global.DbHandler.GetAll<Post>(x => true).OrderByDescending(x => x.PostDate));
        }

        public ActionResult New()
        {
            return View();
        }

        // Defines that this controller should be accessed with post request and that validation should not be enabled.
        // The reason for disabling validation, is that we want to embed HTML within the content of the post.
        [HttpPost, ValidateInput(false)]
        public ActionResult New(Post post)
        {
            User user;
            if (NotAdmin(out user))
            {
                return View("PermissionDenied");
            }
            if (!ModelState.IsValid)
            {
                return View();
            }
            post.PostDate = DateTime.Now;
            post.Author = user.Name;
            Global.DbHandler.Add(post);
            return RedirectToAction("Front", "Blog");
        }

        // Notice that the postUrlTitle defined within routes are passed as an argument
        public ActionResult Posts(string postUrlTitle)
        {
            Post post;
            return NotPost(postUrlTitle, out post) ? View("BadPost") : View(post);
        }

        public ActionResult Delete(string postUrlTitle)
        {
            User user;
            if (NotAdmin(out user))
            {
                return View("BadPost");
            }
            Global.DbHandler.DeleteFirst<Post>(x => x.UrlTitle == postUrlTitle);
            return RedirectToAction("Front", "Blog");
        }

        [HttpPost]
        public ActionResult Comment(string postUrlTitle, string comment)
        {
            Post post;
            if (NotAdmin())
            {
                return View("PermissionDenied");
            }
            if (NotPost(postUrlTitle, out post))
            {
                return View("BadPost");
            }
            if (post.Comments == null)
            {
                post.Comments = new List<string>();
            }

            post.Comments.Add(comment);
            Global.DbHandler.Update(x => x.UrlTitle == postUrlTitle, post);
            return RedirectToAction($"Posts/{postUrlTitle}", "Blog");
        }

        public ActionResult Edit(string postUrlTitle)
        {
            Post post;
            return BadPermission() ?? (NotPost(postUrlTitle, out post) ? View("BadPost") : View(post));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(Post post)
        {
            if (NotAdmin())
            {
                return View("PermissionDenied");
            }
            post.Comments = Global.DbHandler.GetFirst<Post>(x => x.Id == post.Id).Comments;
            Global.DbHandler.Update(x => x.Id == post.Id, post);
            return RedirectToAction("Front", "Blog");
        }
    }
}