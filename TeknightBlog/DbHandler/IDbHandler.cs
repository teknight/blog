using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TeknightBlog.DbHandler
{
    public interface IDbHandler
    {
        void Add<T>(T element);
        T GetFirst<T>(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll<T>(Expression<Func<T, bool>> predicate);
        void DeleteFirst<T>(Expression<Func<T, bool>> predicate);
        void DeleteAll<T>(Expression<Func<T, bool>> predicate);
        void Update<T>(Expression<Func<T, bool>> predicate, T replace);
    }
}