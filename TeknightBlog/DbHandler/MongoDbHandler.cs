using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace TeknightBlog.DbHandler
{
    public class MongoDbHandler : IDbHandler
    {
        public IMongoDatabase Database { get; }

        public MongoDbHandler()
        {
            // Default constructor will look for a MongoDB server running on localhost...
            MongoClient client = new MongoClient();
            
            // The mongoDB driver will create the DB if it does not exist.
            Database = client.GetDatabase("BlogDB"); 
        }

        // We will use the typename to determine the collection name. Bare in mind, that this will remove the option
        // to have different collection of the same type, which may not always be wanted...
        public IMongoCollection<T> GetCollection<T>() => Database.GetCollection<T>(typeof(T).FullName);

        public void Add<T>(T element)
        {
            GetCollection<T>().InsertOne(element);
        }

        public T GetFirst<T>(Expression<Func<T, bool>> predicate) => GetCollection<T>().Find(predicate).FirstOrDefault();
        
        public IEnumerable<T> GetAll<T>(Expression<Func<T, bool>> predicate) =>
            GetCollection<T>().Find(predicate).ToEnumerable();

        public void DeleteFirst<T>(Expression<Func<T, bool>> predicate) => GetCollection<T>().DeleteOne(predicate);
        public void DeleteAll<T>(Expression<Func<T, bool>> predicate) => GetCollection<T>().DeleteMany(predicate);

        public void Update<T>(Expression<Func<T, bool>> predicate, T replace) =>
            GetCollection<T>().ReplaceOne(predicate, replace);
    }
}