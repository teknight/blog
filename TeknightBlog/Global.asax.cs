﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using TeknightBlog.DbHandler;

namespace TeknightBlog
{
    public class Global : HttpApplication
    {
        public static IDbHandler DbHandler { get; private set; }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DbHandler = new MongoDbHandler();
        }
    }
}