﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace TeknightBlog.Models
{
    public class Post
    {
        public string Title { get; set; }
        public int Id => Title.GetHashCode();
        private string _urlTitle;
        public string UrlTitle {
            get
            {
                _urlTitle = Title.Substring(0, Math.Min(Title.Length, 20)).Replace(" ", "-");
                return _urlTitle;
            }
            set { _urlTitle = value; }
        }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public DateTime PostDate { get; set; }
        public List<string> Comments { get; set; }
    }
}