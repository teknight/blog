﻿using System;
using TeknightBlog.Utilities;

namespace TeknightBlog.Models
{
    public class User
    {
        public int Id => Username.GetHashCode();
        public string Username { get; set; }
        private string _passwordHash;
        public string PasswordHash
        {
            get { return _passwordHash; }
            set { _passwordHash = value.Length < 25 ? Hashing.sha256_hash(value) : value; }
        }
        public string Name { get; set; }
        public bool Admin { get; set; }
        public DateTime MemberSince { get; set; }

        public bool CheckPassword(string password) => Hashing.sha256_hash(password) == PasswordHash;
    }
}