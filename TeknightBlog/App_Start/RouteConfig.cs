﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TeknightBlog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                            "HandlePosts",
                            "Blog/{action}/{postUrlTitle}",
                            new {controller = "Blog", action = "Posts"}
                           );

            routes.MapRoute(
                            "Default",
                            "{controller}/{action}/",
                            new {controller = "Blog", action = "Front"}
                           );
        }
    }
}